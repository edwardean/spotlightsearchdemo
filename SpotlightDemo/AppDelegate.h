//
//  AppDelegate.h
//  SpotlightDemo
//
//  Created by Demo on 15/10/10.
//  Copyright © 2015年 Herry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

