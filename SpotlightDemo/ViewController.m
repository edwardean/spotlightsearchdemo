//
//  ViewController.m
//  SpotlightDemo
//
//  Created by Demo on 15/10/10.
//  Copyright © 2015年 Herry. All rights reserved.
//

#import "ViewController.h"
#import "HLCoreSpotlightManager.h"

@interface ViewController ()

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSString *path = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"];
    NSArray *array = [NSArray arrayWithContentsOfFile:path];
    NSMutableArray *items = [NSMutableArray array];
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CSSearchableItemAttributeSet *attributeSet = [[CSSearchableItemAttributeSet alloc] initWithItemContentType:(NSString *)kUTTypeItem];
        attributeSet.title = ((NSDictionary *)obj)[@"title"];
        attributeSet.contentDescription = ((NSDictionary *)obj)[@"name"];;
        attributeSet.identifier = @"demo";
        CSSearchableItem *item = [[CSSearchableItem alloc] initWithUniqueIdentifier:attributeSet.title
                                                                   domainIdentifier:@"demo.domain"
                                                                       attributeSet:attributeSet];
        [items addObject:item];
    }];
    
    
    [HLCoreSpotlightManager hl_indexSpotlightItems:^NSArray<CSSearchableItem *> * _Nullable{
        return [items copy];
    } handler:^(NSError * _Nullable error) {
        
    }];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
