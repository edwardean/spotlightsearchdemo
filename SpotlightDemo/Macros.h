//
//  Macros.h
//  SpotlightDemo
//
//  Created by Demo on 15/10/10.
//  Copyright © 2015年 Herry. All rights reserved.
//

#ifndef Macros_h
#define Macros_h

#if __has_feature(nullability)
#   define __ASSUME_NONNULL_BEGIN      NS_ASSUME_NONNULL_BEGIN
#   define __ASSUME_NONNULL_END        NS_ASSUME_NONNULL_END
#   define __NULLABLE                  __nullable
#else
#   define __ASSUME_NONNULL_BEGIN
#   define __ASSUME_NONNULL_END
#   define __NULLABLE
#endif

#if __has_feature(objc_generics)
#   define __GENERICS(...)             <__VA_ARGS__>
#   define __GENERICS_TYPE(...)        __VA_ARGS__
#else
#   define __GENERICS(class, ...)
#   define __GENERICS_TYPE(...)
#endif

#endif /* Macros_h */
