//
//  HLCoreSpotlightManager.m
//  SpotlightDemo
//
//  Created by Demo on 15/10/10.
//  Copyright © 2015年 Herry. All rights reserved.
//

#import "HLCoreSpotlightManager.h"

static NSError *hl_searchableUnsupportError();
static NSError *hl_searchableItemInvalidError();

static NSString *const kSpotlightSearchableIndexIdentifier = @"your.apotlight.index.identifier";
static HLCoreSpotlightManager *spotlightManager;

@interface HLCoreSpotlightManager ()
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 9000
@property (nonatomic, strong) CSSearchableIndex *defautSpotlightIndex;
#endif
@end

@implementation HLCoreSpotlightManager

+ (HLCoreSpotlightManager *)DefaultCoreSpotlightManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        spotlightManager = [[HLCoreSpotlightManager alloc] init];
    });
    return spotlightManager;
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        spotlightManager = [super allocWithZone:zone];
    });
    return spotlightManager;
}

- (instancetype)init {
    if (self = [super init]) {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 9000
        _defautSpotlightIndex = [[CSSearchableIndex alloc] initWithName:kSpotlightSearchableIndexIdentifier];
#endif
    }
    return self;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 9000
+ (void)hl_indexSpotlightSearchItem:(CSSearchableItem *)item handler:(HLCoreSpotlightFinishHandler)handler {
    if (!item) {
        if (handler) {
            handler(hl_searchableItemInvalidError());
        }
        return;
    }
    [self hl_indexSpotlightItems:^NSArray __GENERICS(CSSearchableItem *)*__NULLABLE {
        return @[item];
    } handler:handler];
}
#endif

+ (void)hl_deleteSpotlightItemWithUniqueIdentifier:(NSString *)identifier handler:(HLCoreSpotlightFinishHandler)handler {
    if (!identifier) {
        if (handler) {
            handler(hl_searchableItemInvalidError());
        }
        return;
    }
    [self hl_deleteSpotlightItemWithUniqueIdentifiers:^NSArray __GENERICS(NSString *)*__NULLABLE {
        return @[identifier];
    } handler:handler];
}

+ (void)hl_deleteSpotlightItemInDomain:(NSString *)domain handler:(HLCoreSpotlightFinishHandler)handler {
    if (!domain) {
        if (handler) {
            handler(hl_searchableItemInvalidError());
        }
        return;
    }
    [self hl_deleteSpotlightItemWithDomains:^NSArray __GENERICS(NSString *)*__NULLABLE {
        return @[domain];
    } handler:handler];
}

+ (void)hl_indexSpotlightItems:(HLCoreSpotlightSearchableItemHandler)attributeSetBlock
                       handler:(HLCoreSpotlightFinishHandler)handler {

    [[HLCoreSpotlightManager DefaultCoreSpotlightManager] indexSpotlightItems:attributeSetBlock handler:handler];
}

+ (void)hl_deleteSpotlightItemWithUniqueIdentifiers:(HLCoreSpotlightDeleteItemByUniqueIdentifierHandler)uniqueIdentifierBlock
                                            handler:(HLCoreSpotlightFinishHandler)handler {

    [[HLCoreSpotlightManager DefaultCoreSpotlightManager] deleteSpotlightItemWithUniqueIdentifiers:uniqueIdentifierBlock
                                                                                           handler:handler];
}

+ (void)hl_deleteSpotlightItemWithDomains:(HLCoreSpotlightDeleteItemByDomainHandler)domainIdentifier
                                  handler:(HLCoreSpotlightFinishHandler)handler {
    [[HLCoreSpotlightManager DefaultCoreSpotlightManager] deleteSpotlightItemWithDomains:domainIdentifier handler:handler];
}

+ (void)hl_deleteAllSpotlightSearchableItemsWithHandler:(HLCoreSpotlightFinishHandler)handler {
    [[HLCoreSpotlightManager DefaultCoreSpotlightManager] deleteAllSpotlightSearchableItemsWithHandler:handler];
}

#pragma mark - private instance methods

- (void)indexSpotlightItems:(HLCoreSpotlightSearchableItemHandler)attributeSetBlock
                    handler:(HLCoreSpotlightFinishHandler)handler {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 9000
    if (!hl_searchableIndexAvailable()) {
        if (handler) {
            handler(hl_searchableUnsupportError());
        }
        return;
    }

    if (!attributeSetBlock) {
        if (handler) {
            handler(hl_searchableItemInvalidError());
        }
        return;
    }

    NSArray *items = attributeSetBlock();
    if (!items) {
        if (handler) {
            handler(hl_searchableItemInvalidError());
        }
        return;
    }

    [self.defautSpotlightIndex indexSearchableItems:items
                                  completionHandler:^(NSError *__NULLABLE error) {
                                      if (handler) {
                                          handler(error);
                                      }
                                  }];
#endif
}

- (void)deleteSpotlightItemWithUniqueIdentifiers:(HLCoreSpotlightDeleteItemByUniqueIdentifierHandler)uniqueIdentifierBlock
                                         handler:(HLCoreSpotlightFinishHandler)handler {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 9000
    if (!hl_searchableIndexAvailable()) {
        if (handler) {
            handler(hl_searchableUnsupportError());
        }
        return;
    }

    if (!uniqueIdentifierBlock) {
        if (handler) {
            handler(hl_searchableItemInvalidError());
        }
        return;
    }

    NSArray *uniqueIdentifiers = uniqueIdentifierBlock();
    if (!uniqueIdentifiers) {
        if (handler) {
            handler(hl_searchableItemInvalidError());
        }
        return;
    }
    [self.defautSpotlightIndex deleteSearchableItemsWithIdentifiers:uniqueIdentifiers
                                                  completionHandler:^(NSError *__NULLABLE error) {
                                                      if (handler) {
                                                          handler(error);
                                                      }
                                                  }];
#endif
}

- (void)deleteSpotlightItemWithDomains:(HLCoreSpotlightDeleteItemByDomainHandler)domainIdentifier
                               handler:(HLCoreSpotlightFinishHandler)handler {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 9000
    if (!hl_searchableIndexAvailable()) {
        if (handler) {
            handler(hl_searchableUnsupportError());
        }
        return;
    }

    if (!domainIdentifier) {
        if (handler) {
            handler(hl_searchableItemInvalidError());
        }
        return;
    }
    NSArray *domainIdentifiers = domainIdentifier();
    if (!domainIdentifiers) {
        if (handler) {
            handler(hl_searchableItemInvalidError());
        }
        return;
    }

    [self.defautSpotlightIndex deleteSearchableItemsWithDomainIdentifiers:domainIdentifiers
                                                        completionHandler:^(NSError *__NULLABLE error) {
                                                            if (handler) {
                                                                handler(error);
                                                            }
                                                        }];
#endif
}

- (void)deleteAllSpotlightSearchableItemsWithHandler:(HLCoreSpotlightFinishHandler)handler {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 9000
    [self.defautSpotlightIndex deleteAllSearchableItemsWithCompletionHandler:^(NSError *__NULLABLE error) {
        if (handler) {
            handler(error);
        }
    }];
#endif
}
@end

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-function"
static BOOL hl_searchableIndexAvailable() {
    if (!NSClassFromString(@"CSSearchableIndex")) {
        return NO;
    }
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 9000
    if (![CSSearchableIndex isIndexingAvailable]) {
        return NO;
    }
#else
    return NO;
#endif
    return YES;
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpointer-bool-conversion"
static NSError *hl_searchableUnsupportError() {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 9000
    if (&CSIndexErrorDomain) {
        return [NSError errorWithDomain:CSIndexErrorDomain code:CSIndexErrorCodeIndexingUnsupported userInfo:nil];
    }
#endif
    return nil;
}

static NSError *hl_searchableItemInvalidError() {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 9000
    if (&CSIndexErrorDomain) {
        return [NSError errorWithDomain:CSIndexErrorDomain code:CSIndexErrorCodeInvalidItemError userInfo:nil];
    }
#endif
    return nil;
}
#pragma clang diagnostic pop
#pragma clang diagnostic pop
