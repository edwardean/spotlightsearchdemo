//
//  HLCoreSpotlightManager.h
//  SpotlightDemo
//
//  Created by Demo on 15/10/10.
//  Copyright © 2015年 Herry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"

static BOOL hl_searchableIndexAvailable();

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 9000
@import CoreSpotlight;
#endif
@import MobileCoreServices;

__ASSUME_NONNULL_BEGIN
typedef NSArray __GENERICS(CSSearchableItem *) * __NULLABLE (^HLCoreSpotlightSearchableItemHandler)(void);
typedef NSArray __GENERICS(NSString *) * __NULLABLE (^HLCoreSpotlightDeleteItemByUniqueIdentifierHandler)(void);
typedef NSArray __GENERICS(NSString *) * __NULLABLE (^HLCoreSpotlightDeleteItemByDomainHandler)(void);
typedef void (^__NULLABLE HLCoreSpotlightFinishHandler)(NSError *__NULLABLE error);

@interface HLCoreSpotlightManager : NSObject

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 9000
+ (void)hl_indexSpotlightSearchItem:(CSSearchableItem *)item handler:(HLCoreSpotlightFinishHandler)handler;
#endif

+ (void)hl_deleteSpotlightItemWithUniqueIdentifier:(NSString *)identifier handler:(HLCoreSpotlightFinishHandler)handler;

+ (void)hl_deleteSpotlightItemInDomain:(NSString *)domain handler:(HLCoreSpotlightFinishHandler)handler;

+ (void)hl_indexSpotlightItems:(HLCoreSpotlightSearchableItemHandler)attributeSetBlock
                       handler:(HLCoreSpotlightFinishHandler)handler;

+ (void)hl_deleteSpotlightItemWithUniqueIdentifiers:(HLCoreSpotlightDeleteItemByUniqueIdentifierHandler)uniqueIdentifierBlock
                                            handler:(HLCoreSpotlightFinishHandler)handler;

+ (void)hl_deleteSpotlightItemWithDomains:(HLCoreSpotlightDeleteItemByDomainHandler)domainIdentifier
                                  handler:(HLCoreSpotlightFinishHandler)handler;

+ (void)hl_deleteAllSpotlightSearchableItemsWithHandler:(HLCoreSpotlightFinishHandler)handler;
@end
__ASSUME_NONNULL_END
